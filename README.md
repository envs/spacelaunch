# spacelaunch

Space Launch is a pubnix take on the classic Unix prank called
Steam Locomotive.  Rather than running a steam engine train
across users' screen when they mistype 'ls' as 'sl', Space
Launch blasts a rocket across their screen.

Space Launch is a collaborative pubnix/tilde project.  It was
started by cmccabe in 2019, but you are encouraged to improve
it.  The code is here: https://tildegit.org/cmccabe/spacelaunch
